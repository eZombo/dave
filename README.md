# DAVE #

An animated short film rendered in Unreal 4

### Notes ###

* You will need at least Unreal 4.11 to open the project
* Starter Content is needed for a few references
* Sometimes Props disappear when using the "Play in editor" feature but everything should function correctly when "Launching" or "Packaging" the Project